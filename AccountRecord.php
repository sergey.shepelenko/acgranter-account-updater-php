<?php

namespace Acgranter\AccountUpdater;

class AccountRecord{
  protected $account = '';
  protected $keys = [];

  /**
   * @param string $account
   */
  public function __construct(string $account = '')
  {
    $this->account = $account;
  }

  /**
   * @param string $account
   */
  public function setAccount(string $account): void
  {
    $this->account = $account;
  }

  /**
   * @return string
   */
  public function getAccount(): string
  {
    return $this->account;
  }

  /**
   * @return array
   */
  public function getKeys(): array
  {
    return $this->keys;
  }

  /**
   * @param array $keys
   */
  public function setKeys(array $keys): void
  {
    $this->keys = $keys;
  }

  /**
   * @param string $key
   */
  public function addKey(string $key): void
  {
    if (!in_array($key, $this->keys)){
      $this->keys[] = $key;
    }
  }
}

