<?php

namespace Acgranter\AccountUpdater\Exception;

class InvalidArgumentException extends \InvalidArgumentException
{
}
