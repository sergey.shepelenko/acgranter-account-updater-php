<?php

namespace Acgranter\AccountUpdater;

use Acgranter\AccountUpdater\Exception\InvalidArgumentException;

class AccountUpdaterFactory
{
  private $accountFiles;

  /**
   * @param array $accountFiles
   */
  public function __construct(array $accountFiles = [])
  {
    $this->accountFiles = $accountFiles;
  }

  public function getAccountUpdaterByFile(string $path): AccountUpdaterInterface
  {
    foreach ($this->accountFiles as $accountFilePath => $accountFileConfig){
      if ($accountFilePath === $path){
        return $this->createAccountUpdater($accountFilePath, $accountFileConfig);
      }
    }
    throw new InvalidArgumentException(sprintf("Account sile '%s' is not configured", $path));
  }

  /**
   * @param $accountFilePath
   * @param $accountFileConfig
   * @return AccountUpdaterInterface
   * @throws Exception\RuntimeException
   * @throws Exception\FileNotFoundException
   */
  private function createAccountUpdater($accountFilePath, $accountFileConfig): AccountUpdaterInterface{
    if (isset($accountFileConfig['account_updater'])){
      /**
       * @var $accountUpdater AccountUpdaterInterface
       */
      $accountUpdater = $accountFileConfig['account_updater'];
      if ( !($accountUpdater instanceof AccountUpdaterInterface) ){
        throw new \RuntimeException(sprintf(
          'Service "%s" does not implement %s.',
          \is_object($accountUpdater) ? get_debug_type($accountUpdater) : $accountUpdater, AccountUpdaterInterface::class
        ));
      }
      return $accountFileConfig['account_updater'];
    }else{
      $type = $accountFileConfig['type'] ?? AccountUpdaterInterface::TYPE_PASSWORD;
      $suffixes = $accountFileConfig['filename_suffixes'] ?? [];
      $csvConfig = $accountFileConfig['csv'] ?? [];
      return new AccountUpdater(
        $accountFilePath, $type,
        $suffixes, $csvConfig
      );
    }
  }
}
