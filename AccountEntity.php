<?php

namespace Acgranter\AccountUpdater;

abstract class AccountEntity implements AccountEntityInterface
{
  public static function getActiveAccountCondition(): array{
    return ['status' => 1];
  }

  public function getAccountBag(): AccountBag{
    return new AccountBag();
  }
}
