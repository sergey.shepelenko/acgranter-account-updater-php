<?php

namespace Acgranter\AccountUpdater;

use Acgranter\AccountUpdater\Exception\FileNotFoundException;
use Acgranter\AccountUpdater\Exception\InvalidArgumentException;
use Acgranter\AccountUpdater\Exception\RuntimeException;

use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Lock\Store\SemaphoreStore;

class AccountUpdater implements AccountUpdaterInterface{
  /**
   * @var string
   */
  protected $path = '';

  /**
   * @var string
   */
  protected $type = '';

  protected $separator = ",";
  protected $enclosure = "\"";
  protected $escape = "\\";
  protected $oldSuffix = ".old";
  protected $newSuffix = ".new";

  /**
   * @var []bool
   */
  protected $accountsToRemove = [];

  /**
   * @var []AccountRecord
   */
  protected $accountsToSet = [];

  /**
   * @var []AccountRecord
   */
  protected $accounts = [];

  protected $mergedContent = '';

  protected $warnings = [];
  protected $errors = [];

  protected $accountSourceContent = null;

  private $lockFactory;

  /**
   * @param string $path
   * @param string $type
   * @throws FileNotFoundException
   * @throws InvalidArgumentException
   */
  public function __construct(string $path, string $type, array $filenameSuffixes = [], array $csvConfig = [])
  {
    if (!is_file($path)){
      throw new FileNotFoundException(sprintf("CSV file path is not accessible: %s", $path));
    }
    if (!in_array($type, [
      self::TYPE_PASSWORD,
      self::TYPE_TOKEN,
      self::TYPE_IP
    ])){
      throw new InvalidArgumentException(sprintf("Unknown CSV file type: %s", $type));
    }
    $this->path = $path;
    $this->type = $type;

    if(isset($filenameSuffixes['old'])){
      $this->oldSuffix = $filenameSuffixes['old'];
    }
    if(isset($filenameSuffixes['new'])){
      $this->newSuffix = $filenameSuffixes['new'];
    }
    if(isset($csvConfig['separator'])){
      $this->separator = $csvConfig['separator'];
    }
    if(isset($csvConfig['enclosure'])){
      $this->enclosure = $csvConfig['enclosure'];
    }
    if(isset($csvConfig['escape'])){
      $this->escape = $csvConfig['escape'];
    }

    $store = new SemaphoreStore();
    $this->lockFactory = new LockFactory($store);
  }

  /**
   * @return string
   */
  public function getPath(): string
  {
    return $this->path;
  }

  /**
   * @param string $path
   */
  public function setPath(string $path): void
  {
    $this->path = $path;
  }

  /**
   * @return string
   */
  public function getType(): string
  {
    return $this->type;
  }

  /**
   * @param string $type
   */
  public function setType(string $type): void
  {
    $this->type = $type;
  }

  public function getMergedContent(): string{
    return $this->mergedContent;
  }

  public function removeAccount(string $account) {
    $key = $this->makeItemKey($account);
    $this->accountsToRemove[$key] = true;
  }

  public function setAccountKeys(string $account, array $accountKeys) {
    $key = $this->makeItemKey($account);
    if (!isset($this->accountsToSet[$key])){
      $this->accountsToSet[$key] = new AccountRecord($account);
    }
    $this->accountsToSet[$key]->setKeys($accountKeys);
  }

  /**
   * Read content from CSV file and convert it into operational array
   */
  public function readAccountsFromFile() {
    $content = file_get_contents($this->path);
    $lines = explode("\n", $content);
    $this->accounts = [];
    foreach ($lines as $line){
      $accountPair = $this->csvLineToAccountPair($line);
      if (null === $accountPair){
        continue;
      }
      $key = $this->makeItemKey($accountPair['account']);
      if (!isset($this->accounts[$key])){
        $this->accounts[$key] = new AccountRecord($accountPair['account']);
      }
      $this->accounts[$key]->addKey($accountPair['key']);
    }
  }

  /**
   * Generates CSV data from operational arrays
   */
  public function merge() {
    if (count($this->accountsToSet) !== 0){
      if (count($this->accounts) === 0){
        $this->accounts = $this->accountsToSet;
      }else{
        foreach ($this->accountsToSet as $key => $item) {
          $this->accounts[$key] = $item;
        }
      }
    }
    if (count($this->accountsToRemove) !== 0 && count($this->accounts) !== 0){
      foreach ($this->accountsToRemove as $key => $sth) {
        unset($this->accounts[$key]);
      }
    }
    $this->mergedContent = $this->generateContent();
  }

  /**
   * Save generated CSV content to file
   *
   * @throws RuntimeException
   *
   * @return array
   */
  public function writeAccountsToFile() {
    $lock = $this->lockFactory->createLock('AccountUpdater' . $this->path);

    if ($lock->acquire(true)) {
      $content = file_get_contents($this->path);

      if ($content !== $this->mergedContent){
        $this->writeFile($this->mergedContent);
      }

      $lock->release();
    }else{
      throw new RuntimeException('Failed to acquire lock');
    }
  }

  public function getErrors():array {
    return $this->errors;
  }

  public function getWarnings():array {
    return $this->warnings;
  }

  protected function makeItemKey(string $account) {
    return base64_encode($account);
  }

  protected function generateContent(): string {
    $lines = [];
    foreach ($this->accounts as $item){
      foreach ($item->getKeys() as $accountKey){
        $lines[] = $this->accountPairToCsvLine($item->getAccount(), $accountKey);
      }
    }
    sort($lines);
    return implode("\n", $lines) . "\n";
  }

  protected function csvLineToAccountPair($line): ?array {
    $row = str_getcsv($line);
    if (count($row) !== 2){
      return null;
    }
    return [
      'account' => $row[0],
      'key' => $row[1]
    ];
  }

  protected function accountPairToCsvLine(string $account, string $accountKey): string {
    $fp = fopen('php://memory', 'r+');
    fputcsv($fp, [$account, $accountKey], $this->separator, $this->enclosure, $this->escape);
    rewind($fp);
    $data = '';
    while (!feof($fp)) {
      $data .= fread($fp, 1048576);
    }
    fclose($fp);
    return rtrim($data, "\n");
  }

  protected function writeFile($data) {
    if ($this->oldSuffix !== ""){
      $bkFilePath = $this->path . $this->oldSuffix;
      try {
        if (!copy($this->path, $bkFilePath)){
          $errors = error_get_last();
          if (!empty($errors)){
            $this->warnings[] = print_r($errors, true);
          }
        }
      }catch (\Throwable $e){
        $this->warnings[] = $e->getMessage();
      }
    }
    if ($this->newSuffix !== ""){
      $tmpFilePath = $this->path . $this->newSuffix;
      try {
        if (file_put_contents($tmpFilePath, $data, LOCK_EX) === strlen($data)) {
          if (!rename($tmpFilePath, $this->path)){
            $errors = error_get_last();
            if (!empty($errors)){
              $this->errors[] = print_r($errors, true);
            }
          }
          return true;
        }
        $this->errors[] = error_get_last();
        unlink($tmpFilePath);
        return false;
      }catch (\Throwable $e){
        $this->errors[] = $e->getMessage();
      }
    }
  }
}
