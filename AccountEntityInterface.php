<?php

namespace Acgranter\AccountUpdater;

interface AccountEntityInterface
{
  /**
   * @return array
   */
  public static function getActiveAccountCondition(): array;

  public function getAccountBag(): AccountBag;
}
