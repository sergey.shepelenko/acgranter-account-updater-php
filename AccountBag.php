<?php

namespace Acgranter\AccountUpdater;

class AccountBag{
  private $account = '';
  private $passwordEnabled = false;
  private $password = '';
  private $tokenEnabled = false;
  private $tokens = [];
  private $ipEnabled = false;
  private $ips = [];

  /**
   * @param string $account
   */
  public function __construct(string $account = '', string $password = '', array $tokens = [], array $ips = [])
  {
    $this->account = $account;
    if ($password !== ''){
      $this->setPassword($password);
    }
    if (count($tokens) !== 0){
      $this->setTokens($tokens);
    }
    if (count($ips) !== 0){
      $this->setIps($ips);
    }
  }

  /**
   * @param string $account
   */
  public function setAccount(string $account): void
  {
    $this->account = $account;
  }

  /**
   * @return bool
   */
  public function isPasswordEnabled(): bool
  {
    return $this->passwordEnabled;
  }

  /**
   * @return bool
   */
  public function isTokenEnabled(): bool
  {
    return $this->tokenEnabled;
  }

  /**
   * @return bool
   */
  public function isIpEnabled(): bool
  {
    return $this->ipEnabled;
  }

  public function enablePassword(): void
  {
    $this->passwordEnabled = true;
  }

  public function disablePassword(): void
  {
    $this->passwordEnabled = false;
    $this->password = '';
  }

  public function enableToken(): void
  {
    $this->tokenEnabled = true;
  }

  public function disableToken(): void
  {
    $this->tokenEnabled = false;
    $this->tokens = [];
  }

  public function enableIp(): void
  {
    $this->ipEnabled = true;
  }

  public function disableIp(): void
  {
    $this->ipEnabled = false;
    $this->ips = [];
  }

  /**
   * @param string $password
   */
  public function setPassword(string $password): void
  {
    $this->password = $password;
    $this->passwordEnabled = true;
  }

  /**
   * @param array $tokens
   */
  public function setTokens(array $tokens): void
  {
    $this->tokens = $tokens;
    $this->tokenEnabled = true;
  }

  /**
   * @param string $token
   */
  public function addToken(string $token): void
  {
    $this->tokens[] = $token;
    $this->tokenEnabled = true;
  }

  /**
   * @param array $ips
   */
  public function setIps(array $ips): void
  {
    $this->ips = $ips;
    $this->ipEnabled = true;
  }

  /**
   * @param string $ip
   */
  public function addIp(string $ip): void
  {
    $this->ips[] = $ip;
    $this->ipEnabled = true;
  }

  /**
   * @return string
   */
  public function getAccount(): string
  {
    return $this->account;
  }

  /**
   * @return string
   */
  public function getPassword(): string
  {
    return $this->password;
  }

  /**
   * @return array
   */
  public function getTokens(): array
  {
    return $this->tokens;
  }

  /**
   * @return array
   */
  public function getIps(): array
  {
    return $this->ips;
  }

}
