<?php

namespace Acgranter\AccountUpdater;

interface AccountUpdaterInterface
{
  const TYPE_PASSWORD = 'password';
  const TYPE_TOKEN = 'token';
  const TYPE_IP = 'ip';

  public function getPath(): string;

  public function setPath(string $path): void;

  public function getType(): string;

  public function setType(string $type): void;

  public function getMergedContent(): string;

  public function removeAccount(string $account);

  public function setAccountKeys(string $account, array $accountKeys);

  public function readAccountsFromFile();

  public function merge();

  public function writeAccountsToFile();

  public function getErrors():array;

  public function getWarnings():array;
}
